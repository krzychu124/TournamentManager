package pl.sdacademy.tournamentmanager;

import pl.sdacademy.tournamentmanager.Entities.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayersGenerator {

    public PlayersGenerator() {
    }

    public void generate(int numberOfPlayers){
        HibernateDao playerDao = new HibernateDao();
        for (int i = 0; i < numberOfPlayers; i++) {
            playerDao.add(new Player("firstname" +i, "lastname" +i, "username" +i));
        }
    }

    public List<Player> generatePla(int numberOfPlayers) {
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++) {
            Player player = new Player("firstname" + i, "lastname" + i, "username" + i);
            players.add(player);
        }
        return players;
    }
}
