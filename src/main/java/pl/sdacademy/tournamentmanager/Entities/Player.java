package pl.sdacademy.tournamentmanager.Entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "first_name")
    private String  firstName;
    @Column(name = "last_name")
    private String lastName;
    private String username;
    @OneToMany
    private Set<Match> matchesAsP1 = new HashSet<>();
    @OneToMany
    private Set<Match> matchesAsP2 = new HashSet<>();

    public Player(String firstName, String lastName, String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public Player() {
    }

    public Set<Match> getMaches(){
        Set<Match> allMatches = new HashSet<>();
        allMatches.addAll(matchesAsP1);
        allMatches.addAll(matchesAsP2);
        return allMatches;
    }

    public void addMatchAsPlayer1(Match match) {
        matchesAsP1.add(match);
    }

    public void addMatchAsPlayer2(Match match) {
        matchesAsP2.add(match);
    }

    public int getId(){
        return id;
    }
}
