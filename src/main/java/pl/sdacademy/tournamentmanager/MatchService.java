package pl.sdacademy.tournamentmanager;

import org.hibernate.Session;
import pl.sdacademy.tournamentmanager.Entities.Match;
import pl.sdacademy.tournamentmanager.Entities.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MatchService {

    private Random random = new Random();
    private int roundCounter = 0;

    public List<Match> generateNextRound() {
        List<Match> nextRound = new ArrayList<>();
        List<Player> playerList = getPlayersLeft();
        roundCounter++;

        while (!playerList.isEmpty()) {
            Player player1 = playerList.get(random.nextInt(playerList.size()));
            playerList.remove(player1);
            Player player2 = null;
            if (playerList.size() > 0) {
                player2 = playerList.get(random.nextInt(playerList.size()));
                playerList.remove(player2);
            }
            if (player2 == null) {
                player2 = new Player("walkower", "walkower", "walkower");
                Match nextMatch = new Match(player1, player2, roundCounter);
                nextMatch.setFinalScore(7, 0);
                nextRound.add(nextMatch);
            } else {
                Match nextMatch = new Match(player1, player2, roundCounter);
                nextRound.add(nextMatch);
            }
        }
        return nextRound;
    }

    public List<Player> getPlayersLeft(){
        Session s =SessionFactoryManager.getSessionFactory().openSession();
        List<Player> playerList = s.createQuery(
                "Select" +
                " player.* " +
                        "from " +
                        "player as P " +
                        "left join" +
                        "Match_Table as M on M.id = P.id" +
                        "where " +
                        "M.finished = true" +
                        "and " +
                        "M.winnerId = P.id")
                .list();
        s.close();
        return playerList;
    }
}
