package pl.sdacademy.tournamentmanager;

import pl.sdacademy.tournamentmanager.Entities.Player;
import org.hibernate.Session;

public class HibernateDao {
    private Session session;

    public void add(Player player) {
        session = SessionFactoryManager.getSessionFactory().openSession();
        session.save(player);
        session.close();
    }

    public void update(Player player) {
        session = SessionFactoryManager.getSessionFactory().openSession();
        session.update(player);
        session.close();
    }

    public Player get(int id) {
        session = SessionFactoryManager.getSessionFactory().openSession();
        Player player = session.get(Player.class, id);
        session.close();
        return player;
    }

    public void delete(int id) {
        session = SessionFactoryManager.getSessionFactory().openSession();
        Player player = session.get(Player.class, id);
        session.delete(player);
        session.close();
    }
}
