package pl.sdacademy.tournamentmanager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.List;

public class Main extends Application {
    public static void main(String[] args) {
        PlayersGenerator playersGenerator = new PlayersGenerator();
        playersGenerator.generate(10);
        List idList = SessionFactoryManager.getSessionFactory().openSession().createQuery("Select P.id from Player as P").list();
        idList.forEach(System.out::println);
        SessionFactoryManager.getSessionFactory().close();

        //----------------------
        launch(args);
        //----------------------
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/MainView.fxml"));
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Tournament Manager");
        primaryStage.show();
    }
}
